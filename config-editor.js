var fs = require('fs');
var utility = require('./lib/utility');
var readlineSync = require('readline-sync');

var configManager = {};

function configureOtherConfiguration(config, field){
    if (!utility.checkCondition(config, field)) {
        return;
    }

    if (config != undefined) {
        if (typeof config == field["type"]) {
            return config;
        }
    }

    var answer = null;
    while (true) {
        if(field["private"] != undefined && field["private"] == true){
            answer = readlineSync.question(utility.generateQuestion(field["question"], field["default"]), {noEchoBack: true});
        }
        else{
            answer = readlineSync.question(utility.generateQuestion(field["question"], field["default"]));
        }

        /* Default value */
        if (answer == "") {
            if (field["default"] != undefined) {
                answer = field["default"];
            }
            else {
                console.log("Une valeur est nécessaire.");
                continue;
            }
        }

        /* Handle conversion */
        if (field["type"] == "number") {
            answer = Number(answer);
            if (isNaN(answer)) {
                console.log("Valeur incorrecte.");
                continue;
            }
        }

        else if(field["type"] == "boolean") {
            if(answer == "false"){
                answer = false;
            }
            else if(answer == "true"){
                answer = true;
            }
            else{
                console.log("Valeur non autorisé. Possibilité : true, false");
                continue;
            }
        }

        /* Handle enumeration */
        if (field["values"] != undefined) {
            if (utility.isInJSONArray(answer, field["values"])) {
                break;
            }
            else {
                console.log("Valeur non autorisé. Possibilité : " + field["values"]);
            }
        }
        else {
            break;
        }
    }

    return answer;
}

function configureArrayConfiguration(config, descriptor){
    if(descriptor["limit"] < 0){
        var answer = null;
        while (true) {
            answer = readlineSync.question(descriptor["question-first"]);
            if(answer != "yes" && answer != "no"){
                console.log("Vous devez répondre par yes ou no.")
                continue;
            }
            else{
                break;
            }
        }
        if(answer == "no"){
            return;
        }
        else{
            while(true) {
                if (descriptor["descriptor"]["type"] == "array") {
                    config.push([]);
                    configureArrayConfiguration(config[config.length - 1], descriptor["descriptor"]["descriptor"]);
                }
                else if (descriptor["descriptor"]["type"] == "object") {
                    config.push({});
                    configureObjectConfiguration(config[config.length - 1], descriptor["descriptor"]["descriptor"]);
                    console.log(config[config.length - 1]);
                }
                else {
                    config.push(configureOtherConfiguration(null, descriptor["descriptor"]));
                }
                answer = null;
                while(true) {
                    answer = readlineSync.question(descriptor["question"]);
                    if (answer != "yes" && answer != "no") {
                        console.log("Vous devez répondre par yes ou no.");
                        continue;
                    }
                    else {
                        break;
                    }
                }
                if(answer == "no"){
                    break;
                }
            }
        }
    }
    else{
        for(var index = 1; index <= descriptor["limit"]; index++){
            if (descriptor["descriptor"]["type"] == "array") {
                config.push([]);
                configureArrayConfiguration(config[config.length - 1], descriptor["descriptor"]["descriptor"]);
            }
            else if (descriptor["descriptor"]["type"] == "object") {
                config.push({});
                configureObjectConfiguration(config[config.length - 1], descriptor["descriptor"]["descriptor"]);
                console.log(config[config.length - 1]);
            }
            else {
                config.push(configureOtherConfiguration(null, descriptor["descriptor"]));
            }
        }
    }
}

function configureObjectConfiguration(config, descriptor){
    for (var key in descriptor) {
        var field = descriptor[key];

        if (!utility.checkCondition(config, field)) {
            continue;
        }

        if (field["type"] == "object") {
            if(config[key] == undefined){
                config[key] = {};
            }
            configureObjectConfiguration(config[key], descriptor[key]["descriptor"]);
            continue;
        }
        else if(field["type"] == "array") {
            if(config[key] == undefined){
                config[key] = [];
            }
            configureArrayConfiguration(config[key], descriptor[key]);
            continue;
        }
        else{
            config[key] = configureOtherConfiguration(config[key], field);
        }
    }
}

configManager.configure = function(configPath, descriptorPath) {
    var descriptor = require(process.cwd() + "/" + descriptorPath);
    var config = {};
    try {
        config = require(process.cwd() + "/" + configPath);
    }
    catch (err) {
    }

    configureObjectConfiguration(config, descriptor);

    fs.writeFileSync(process.cwd() + "/" + configPath, JSON.stringify(config, null, 4));
};

module.exports = configManager;
var utility = {};

utility.isInJSONArray = function(value, elements){
    for(var index in elements){
        if(value == elements[index]){
            return true;
        }
    }
    return false;
};

utility.generateQuestion = function(question, defaultValue){
    return question + (defaultValue==undefined?"":" (" + defaultValue+ ")") + " : ";
};

utility.checkCondition = function(config, field){
    if(field["condition"] != undefined){
        if(config[field["condition"]["field"]] != field["condition"]["value"]){
            return false;
        }
    }
    return true;
};

module.exports = utility;
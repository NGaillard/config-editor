Config-editor
=========

A module to write and edit a configuration from a description file.

## Installation

  npm install config-editor --save

## Usage

  var configEditor = require('config-editor');
  configEditor.configure('config.json', 'config-descriptor.json');
  
## Syntax of config descriptor

See the wiki.

## Release History

* 0.1.0 Initial release